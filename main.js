const findPrefix = (strs = []) => {
    if (!validateArray(strs)) return "";

    let prefix = strs[0];
    for (let i = 1; i < strs.length; i++) {
        while (!strs[i].startsWith(prefix)) {
            prefix = prefix.slice(0, -1);
            if (!prefix) return ""; // No common prefix found
        }
    }

    return prefix;
};

const validateArray = (strs = []) => {
    if (!Array.isArray(strs) || strs.length === 0 || strs.length > 200) {
        console.log("Invalid array");
        return false;
    }

    const lowerCaseRegex = /^[a-z]+$/;

    for (const str of strs) {
        if (typeof str !== "string" || str.length > 200 || !lowerCaseRegex.test(str)) {
            console.log("Invalid string: must be lower-case English letters only and max 200 characters");
            return false;
        }
    }

    return true;
};

// Example usage:
// const strs = [];
const strs = ["flower", "flow", "flight"];
// const strs2 = ["dog", "racecar", "car"];
console.log(findPrefix(strs));
